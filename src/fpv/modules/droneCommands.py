# Standard Imports
from typing import Tuple
# Third Party Imports
from numpy import zeros, where
from scipy.linalg import norm
# Local Imports
from .drone import Drone
from .droneOrientation import orientDroneToMap
from .rotations import rotateVector3
from .localFunctions import estimateGateIDs, estimateMeasuredGateRange
from .utils import calcAngleBetweenVectors


def moveDrone(drone: Drone, moveDistance: float, error: float = 0.0) -> Drone:
    """This function moves the drone forward as specified by moveDistance, you.

    can also artificially add error with the error argument

    #Description:
    #This function moves the drone forward as specified by moveDistance, you
    #can also artificially add error with the error argument

    #Step 1: Input argument check. If no simulated error is given as an argument, it sets it to zero

    #Step 2: Moves the simulated drone forward by the defined step size until the specified
    #distance is traversed, some error can be optionally added so the model can
    #be tested under more realistic conditions (In reality, things like wind or
    #imperfect position estimations will throw off your drone's movements).

    #Step 3: Updates the camera's perspective once the simulated drone has been moved
    #for the current step

    #Step 4: Updates the global variable - assumes zero error

    #Step 5: Marks the simulated drone's new location in the simulated environment

    #Input Variables:
    #    moveDistance - The distance you want the drone to move forward. If
    #    given a negative number, the drone will move backwards.
    #           Size: [1 x 1]
    #           Units: meters
    #    error - Artificial error to be added into the distance the drone is
    #    to move. This can be added to test autonomous drone flight in more
    #    realistic scenarios.
    #           Size: [1 x 1]
    #           Units: meters

    #Returned Variables:
    #    NONE

    #NOTE: The drone will move backwards if the given moveDistance (plus
    #error) is negative

    #Author: Kevin Schroeder, PhD
    #Date:  31 May 2021
    """
    # Step 2: Incrementally move the camera the desired distance
    drone.location += drone.heading * (moveDistance + error)

    return drone


def turnDrone(drone: Drone, turnAngle: float, error: float = 0.0) -> Drone:
    """Turn Drone.

    #Description:
    #This function alters the drone's heading by the specified number of
    #degrees. You can artificially add error with the error argument

    #Step 1: Input argument check. If no simulated error is given as an argument, it sets it to zero

    #Step 2: Turns the drone by the specified angle, this is done in steps to give
    #the simulated drone more realistic movement. The for loop smooths out the
    #visualization as we move from one heading to another.

    #Step 3: Updates the camera's perspective to be that of the drone after the drone
    #has finished rotating for this step

    #Step 4: Updates the global variable ESTIMATEDDRONEHEADING - assumes zero error

    #Input Variables:
    #    turnAngle - The angular distance you would like the drone to rotate
    #    clockwise. If given a negative angle, the drone will rotate
    #    counterclockwise
    #           Size: [1 x 1]
    #           Units: degrees
    #    error - Artificial error to be added into the distance the drone is
    #    to move. This can be added to test autonomous drone flight in more
    #    realistic scenarios.
    #           Size: [1 x 1]
    #           Units: degrees

    #Returned Variables:
    #    NONE

    #NOTE: The drone will rotate clockwise if the given turn angle (plus
    #error) is negative

    #Author: Kevin Schroeder, PhD
    #Date:  31 May 2021
    """
    drone.heading = drone.heading.dot(rotateVector3(turnAngle + error))

    return drone


def updateDrone(estimate_drone: Drone, true_drone: Drone) -> Tuple[Drone, Drone]:
    """Update Drone.

    #   Description:
    #   This function is called each time the drone moves or turns. It is used
    #   to update the 'ESTIMATEDDRONELOCATION' and 'ESTIMATEDDRONEHEADING' based
    #   on any identified gate in view of the Drone's camera.
    #
    #   Input Variables:
    #      NONE
    #
    #   Returned Variables:
    #       NONE
    #
    #   Author: Kevin Schroeder, PhD
    #   Date:  31 May 2021
    """
    # Step 1: Identify gates in drone's FOV
    allProjectedCorners, gateIDs, _ = estimateGateIDs(estimate_drone, true_drone)
    try:
        numberOfGates = gateIDs.shape[0]
    except AttributeError:
        numberOfGates = 0

    # Step 2: Identify and localized based on the closest gate
    if numberOfGates != 0:
        ranges = zeros((numberOfGates, ))
        for ii in range(numberOfGates):
            ranges[ii] = estimateMeasuredGateRange(allProjectedCorners[ii])

        closestGate = where(ranges == min(ranges))[0]
        measuredDroneLocation, measuredDroneHeading = orientDroneToMap(allProjectedCorners[closestGate], int(gateIDs[closestGate]))

        # Step 3: If the measured drone location is close to the previous estimate update the estimate
        locationResiduals = measuredDroneLocation - estimate_drone.location
        headingResidual = calcAngleBetweenVectors(measuredDroneHeading, estimate_drone.heading)

        if norm(locationResiduals) < 10 and norm(headingResidual) < 10:
            estimate_drone.location = measuredDroneLocation
            estimate_drone.heading = measuredDroneHeading

    return estimate_drone, true_drone
