"""RESONAATE Package setup file."""
import setuptools

setuptools.setup(
    name="fpv",
    description="FPV Drone Simulator",
    version="0.0.0",
    packages=setuptools.find_packages('src'),
    package_dir={'': 'src'},
    package_data={},
    install_requires=[
        "numpy>=1.19",
        "scipy>=1.6",
        "matplotlib>=3.3",
    ],
    entry_points={
        'console_scripts': [
            'fpv-sim=fpv.driver:main',
        ]
    }
)
