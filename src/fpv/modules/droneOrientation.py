# Standard Imports
from typing import Tuple
# Third Party Imports
from numpy import array, ndarray
# Local Imports
from .globalFunctions import getGateLocation, getGatePose
from .localFunctions import estimateGateLocation, estimateGatePose
from .rotations import rotateFrame3
from .utils import calcAngleBetweenVectors


def orientDroneToGate(projectedCorners: ndarray) -> Tuple[ndarray, ndarray]:
    """Orient Drone to gate.

    #   Description:
    #   This function takes in the pan and tilt corners of a gate and will return
    #   the Drone's location and orientation in a gate-centric frame. It is
    #   expected that this function will be used in conjunction with
    #   'orientDroneToMap' to estimate the drone's location within the Map.
    #
    #   Input Variables:
    #       projectedCorners - Projected corners of the gate
    #              Size: [numCorners x panTilt]
    #                       'numCorners' corresponds to the number of corners (corner
    #                       number, clockwise starting at lower left corner,
    #                       created in 'buildGate'). Typically there will always
    #                       be 4 corners.
    #                       'panTilt' will always have a dimension of two. Dimension
    #                       one corresponds to the pan angles and dimension two to
    #                       the tilt angles.
    #                       NOTE: The first column will always be the pan angle
    #                       and the second will be tilt.
    #              Units: degrees
    #
    #   Returned Variables:
    #       droneRelativeLocation - A vector from the center of the gate to the Drone
    #              Size: [1 x dimension]
    #                    'dimension' corresponds to the right, forward, and up directions;
    #                    it will always be 3.
    #              Units: Meters
    #              Reference Frame: Gate's Local RFU
    #       droneRelativeOrientation -  a unit vector of the drone's orientation, points
    #               along the FPV camera's boresight as viewed from the gate.
    #              Size: [1 x dimension]
    #                    'dimension' corresponds to the right, forward, and up directions;
    #                    it will always be 3.
    #              Units: unitless
    #              Reference Frame: Gate's Local RFU
    #
    #   Author: Kevin Schroeder, PhD
    #   Date:  31 May 2021
    """
    # Step 1: Estimate gate's location and orientation
    droneLookVector = array([0, 1, 0])
    gateRelativeLocation = estimateGateLocation(projectedCorners)
    gatePose = estimateGatePose(projectedCorners)
    # Step 2: Design a frame transformation from the drone frame to the gate frame
    drone2GateAngularOffset = calcAngleBetweenVectors(droneLookVector, gatePose)
    drone2Gate = rotateFrame3(drone2GateAngularOffset)
    # Step 3: Convert Drone measurements to a local gate-based frame
    # NOTE: vecDrone2Gate = -1 * vecGate2Drone ---------
    droneRelativeLocation = -gateRelativeLocation.dot(drone2Gate)
    droneRelativeOrientation = droneLookVector.dot(drone2Gate)

    return droneRelativeLocation, droneRelativeOrientation


def orientDroneToMap(projectedCorners: ndarray, gateID: int) -> Tuple[ndarray, ndarray]:
    """Orient Drone to map.

    #   Description:
    #   This function calculates where the drone's location on the course map
    #   based on its relationship to a known gate in its field of view. This
    #   function assigns the provide gateID to the projectedCorners, and
    #   combines it with information from the MATLAB GLOBAL workspace to estimate
    #   the drone's location and orientation on the course map.
    #   Unlike 'gettrueDroneLocation' and 'gettrueDroneHeading,' this function is only
    #   an estimate. If the gateID provided does not correspond to the
    #   projectedCorners the resulting location and heading will be erroneous.
    #
    #   Input Variables:
    #       projectedCorners - Projected corners of the gate
    #              Size: [numCorners x panTilt]
    #                       'numCorners' corresponds to the number of corners (corner
    #                       number, clockwise starting at lower left corner,
    #                       created in 'buildGate'). Typically there will always
    #                       be 4 corners.
    #                       'panTilt' will always have a dimension of two. Dimension
    #                       one corresponds to the pan angles and dimension two to
    #                       the tilt angles.
    #                       NOTE: The first column will always be the pan angle
    #                       and the second will be tilt.
    #              Units: degrees
    #       gateID - Gate identification number, created in 'buildCourse'. A
    #           gate's identification number dictates what order the drone will fly
    #           through the gate
    #              Size: [1 x 1]
    #
    #   Returned Variables:
    #       droneGlobalLocation - a vector of the drone's current location, points
    #           from the origin of the map to the drone.
    #              Size: [1 x dimension]
    #                    'dimension' corresponds  to the right, forward, and up directions
    #                    it will always be 3.
    #              Units: Meters
    #              Reference Frame: Map's Global RFU
    #       droneGlobalOrientation - a unit vector of the drone's orientation, points
    #           along the FPV camera's boresight
    #              Size: [1 x dimension]
    #                    'dimension' corresponds  to the right, forward, and up directions
    #                    it will always be 3.
    #              Units: unitless
    #              Reference Frame: Map's Global RFU
    #
    #
    #   Author: Kevin Schroeder, PhD
    #   Date:  31 May 2021
    """
    # Step 1: Estimate drone's location relative to a gate
    # NOTE: The "Relative" vector will be in a gate-based coordinate frame
    droneRelativeLocation, droneRelativeOrientation = orientDroneToGate(projectedCorners[0])
    # Step 2: Look up the gate's location and orientation on the map
    # NOTE: The Gate's location and orientation from the map will be in the
    # "Global" coordinate frame
    gateGlobalLocation = getGateLocation(gateID)
    gatePose = getGatePose(gateID)
    gateGlobalOrientation = calcAngleBetweenVectors([0, 1, 0], gatePose)
    # Step 3: Estimate drone's location and orientation on the map
    # Note1: Because `droneRelativeLocation` is not in the "Global" frame it
    # must be rotated before it can be added to 'gateGlobalLocation'

    # Note2: 'gateGlobalOrientation' is the angle from the "Global" forward
    # direction to the gate's forward direction. The opposite angle can be
    # used to make a rotation matrix from the Gate "Relative" frame to the
    # "Global" frame
    gate2Map = rotateFrame3(-gateGlobalOrientation)
    droneGlobalLocation = gateGlobalLocation + droneRelativeLocation.dot(gate2Map)
    droneGlobalOrientation = droneRelativeOrientation.dot(gate2Map)

    return droneGlobalLocation, droneGlobalOrientation
