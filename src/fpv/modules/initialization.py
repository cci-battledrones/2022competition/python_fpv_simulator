# Standard Imports
from typing import Tuple
# Third Party Imports
from numpy import zeros, array, concatenate, ones, ndarray
# Local Imports
from .rotations import rotateVector3
from .utils import calcRandomNoise
from .constants import SIDELENGTH
from .drone import Drone


def buildGate() -> ndarray:
    """Build the gate.

    #   This function generates a generic 3D representation  of a gate for the
    #   BattleDrone's obstacle course. The gate consist of four corners of a
    #   specific size. The corner locations a placed in the XZ plane and
    #   centered about the origin so they can be rotated and translated directly
    #   into their place on the course map.
    #
    #   For Example: gatesCorners = corners*rotateVector3(angle) + [gateLocation];
    #
    #   Input Variables:
    #       NONE
    #
    #   Returned Variables:
    #       corners - a list of vectors pointing to the locations of a gate's corners
    #              Size: [numCorners x dimension]
    #                    where numCorners is number of gate corners
    #                    and 'dimension' corresponds to the right, forward, and
    #                    up directions and will always have a length of 3.
    #              Units: meters
    #              Reference Frame: Map's Global RFU
    #
    #   Author: Kevin Schroeder, PhD
    #   Date:  31 May 2021
    """
    # Create gate template
    gateType = array([[0,0,0],
                      [0,0,1],
                      [1,0,1],
                      [1,0,0]]) * SIDELENGTH
    # Center template on origin
    corners = gateType - ones((4, 3)) * array([1, 0, 1]) * SIDELENGTH / 2
    return corners


def buildCourse() -> ndarray:
    """Build the course.

    #   This function generates a 3D environment that is representative of the
    #   BattleDrone's obstacle course in a MATLAB figure. The 3D environment is
    #   for a FPV simulation where the camera of MATLAB axes can be moved and
    #   oriented to emulate the motion of FPV drone. This function plots the
    #   VT Drone Park and places the 10 gates in their competition locations.
    #   The figure handle is saved in the MATLAB global workspace.
    #
    #   Input Variables:
    #       NONE
    #
    #   Returned Variables:
    #       NONE
    #
    #   Author: Kevin Schroeder, PhD
    #   Date:  31 May 2021
    """
    # Step 1: create gate template
    gateType = buildGate()
    # Step 2: Place gates on the course
    # create course template (rotate and translate)
    gates = zeros((10,4,3))
    gates[0,:,:] = gateType + array([25,-10, SIDELENGTH / 2 + 1])
    gates[1,:,:] = gateType + array([25,10, SIDELENGTH / 2 + 1])
    gates[2,:,:] = gateType + array([-10,30, SIDELENGTH / 2 + 1])
    gates[3,:,:] = gateType.dot(rotateVector3(330)) + array([25, 50, SIDELENGTH / 2 + 1])
    gates[4,:,:] = gateType.dot(rotateVector3(30))  + array([25, 65, SIDELENGTH / 2 + 1])
    gates[5,:,:] = gateType.dot(rotateVector3(150)) + array([-25, 65, SIDELENGTH / 2 + 1])
    gates[6,:,:] = gateType.dot(rotateVector3(210)) + array([-25, 50, SIDELENGTH / 2 + 1])
    gates[7,:,:] = gateType.dot(rotateVector3(180)) + array([10, 30, SIDELENGTH / 2 + 1])
    gates[8,:,:] = gateType.dot(rotateVector3(180)) + array([-25, 10, SIDELENGTH / 2 + 1])
    gates[9,:,:] = gateType.dot(rotateVector3(180)) + array([-25, -10, SIDELENGTH / 2 + 1])

    # Step 3: Create a map of gate locations in the GLOBAL Workspace
    return gates


COURSEMAP = buildCourse()


def initializeDrone(flightAltitude: float) -> Tuple[ndarray, ndarray]:
    """Build the drone."""
    # Instantiate estimated drone location
    estimate_heading = array([0, 1, 0])
    estimate_loc = array([25, -20, flightAltitude])
    estimate_drone = Drone(heading=estimate_heading, location=estimate_loc)

    # Instantiate true drone location
    true_heading = estimate_heading.dot(rotateVector3(calcRandomNoise(5)))
    true_loc = estimate_loc + concatenate((calcRandomNoise(0.5, 2), [0]))
    true_drone = Drone(heading=true_heading, location=true_loc)

    return estimate_drone, true_drone
