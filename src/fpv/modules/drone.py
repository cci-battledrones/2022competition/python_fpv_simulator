from dataclasses import dataclass
from numpy import ndarray


@dataclass
class Drone:
    """Simple definition of a Drone's attributes for easy tracking."""

    heading: ndarray
    location: ndarray
