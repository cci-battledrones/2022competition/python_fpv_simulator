# Standard Library Imports
from typing import Optional, Union, Tuple
# Third Party Imports
from numpy import cross, array, dot, arccos, arcsin, sign, degrees, tan, radians, fabs, clip, finfo, ndarray
from numpy.linalg import norm
from numpy.random import normal
# Local Imports
from .constants import SIDELENGTH
from .rotations import rotateVector1, rotateVector3


def fpe_equals(value: float, expected: float) -> float:  # pylint: disable=invalid-name
    r"""Utility for checking that `value` and `expected` are equal within the bounds of floating point error (FPE).

    Args:
        value (``float``): Value being compared.
        expected (``float``): Value that `value` is expected to be without FPE

    Returns:
        ``bool``: If `True`, then it should be safe to assume that `value == expected` and the `value` is just
        misrepresented due to FPE.
    """
    return fabs(value - expected) < finfo(float).resolution  # pylint: disable=no-member


def safeArccos(arg: float) -> float:
    r"""Safely perform an :math:`\arccos{}` calculation.

    It's possible due to floating point error/truncation that a value that should be
    1.0 or -1.0 could end up being e.g. 1.0000000002 or -1.000000002. These values result
    in ``numpy.arccos()`` throwing a domain warning and breaking functionality. ``safeArccos()``
    checks to see if the given ``arg`` is within the proper domain, and if not, will correct
    the value before performing the :math:`\arccos{}` calculation.

    Raises:
        ``ValueError``: if the given ``arg`` is *actually* outside the domain of :math:`\arccos{}`

    Args:
        arg (``ndarray``): value(s) to perform :math:`\arccos{}` calculation on

    Returns:
        (``ndarray``): result of :math:`\arccos{}` calculation on given ``arg``, radians
    """
    # Check for valid arccos domain
    if fabs(arg) <= 1.0:
        return arccos(arg)
    # else
    if not fpe_equals(fabs(arg), 1.0):
        msg = f"`safeArccos()` used on non-truncation/rounding error. Value: {arg}"
        raise ValueError(msg)
    return arccos(clip(arg, -1, 1))


def calcAngleBetweenVectors(vec1: ndarray, vec2: ndarray) -> float:
    """Calculate Angle Between Vectors.

    #   Description:
    #       This function calculates the angle between two vectors. The
    #       function will either use an optionally provided quadrant check or
    #       will calculate it's own. if no quadrant check is given, the check
    #       is determained by assuming CW and CCW rotation is determained based
    #       on the local z-axis.
    #
    #   Input Variables:
    #       vec1 - the first vector
    #              Size: [1 x 3]
    #       vec2 - the second vector
    #              Size: [1 x 3]
    #       check (optional) - the quadrant check (+1 or -1)
    #              Size: [1 x 1]
    #              Units: unitless
    #
    #   Returned Variables:
    #       angle - the absolute angle between the given vectors
    #              Size: [1 x 1]
    #              Units: degrees
    #
    #   Author: Kevin Schroeder, PhD
    #   Date:  31 May 2021
    #
    #   NOTE: This function utilizes the function 'acosd' which has a domain of
    #         [0, 180]. A quadrant check (+1 or -1) is added to expand the
    #         domain to [-180, +180]
    """
    # Step 1: Assume rotation around z-axis
    rotation = cross(vec1, vec2)
    if rotation[2] == 0:
        check = 1
    else:
        up_vector = array([0, 0, 1])
        check = sign(dot(up_vector, rotation))

    # Step 2: Throw warning if check is not +1 or -1
    if norm(check) != 1:
        check = sign(check)

    # Step 3: Use the definition of a dot product to solve for the angle between two vectors
    cos_angle = dot(vec1, vec2) / (norm(vec1) * norm(vec2))
    angle = check * degrees(safeArccos(cos_angle))
    angle = angle.real

    return angle


def calcPanTilt(look_vector: ndarray, point_vector: ndarray) -> Tuple[float, float]:
    """Calculate Pan Tilt.

    #   Description:
    #       This function calculates the pan and tilt angles of a point in 3D
    #       space from the drone's current forward direction.
    #
    #   Input Variables:
    #       lookVector - the drone's current heading vector.
    #              Size: [1 x 3]
    #                    Typically the lookVector will always be [0 1 0].
    #              Reference Frame: Drone's local RFU
    #       pointVector - a unit vector from the drone to an arbitrary point in space
    #              Size: [1 x 3]
    #              Reference Frame: Drone's local RFU
    #
    #   Returned Variables:
    #       pan -  the angle across the drone's local horizon from the drone's
    #              current heading to the provided point in 3D space. Positive
    #              angles are CCW rotations (defined by the Right-hand Rule).
    #              Size: [1 x 1]
    #              Units: degrees
    #       tilt - the angle off the drone's local horizon from the drone's
    #              current heading to the provided point in 3D space. Positive
    #              angles are above the horizon (defined by the Right-hand Rule).
    #              Size: [1 x 1]
    #              Units: degrees
    #
    #   Author: Kevin Schroeder, PhD
    #   Date:  31 May 2021
    """
    # Step 1: project vectors onto the local horizon plane
    projected_look_vector = look_vector.copy()
    projected_look_vector[2] = 0
    projected_point_vector = point_vector.copy()
    projected_point_vector[2] = 0
    # Step 2: Calculate angle across the horizon
    pan = calcAngleBetweenVectors(projected_look_vector, projected_point_vector)
    # Step 3: Calculate angle off the horizon
    tilt = degrees(arcsin(point_vector[2]))

    return pan, tilt


def calcPose(corners: ndarray) -> ndarray:
    """Calculate Pose.

    #   Description:
    #       This function returns a unit vector pointing in the normal
    #       direction of a gate entrance.
    #
    #   Input Variables:
    #       corners - a list of vectors pointing to the locations of a gate's corners
    #              Size: [numCorners x dimension]
    #                    where numCorners = number of gate corners
    #                    and dimension is always equal to 3
    #              Units: meters
    #              Reference Frame: Global
    #
    #   Returned Variables:
    #       poseVector - a unit vector normal to the plane of the gate pointing
    #       out the front of the gate.
    #              Size: [1 x 3]
    #              Units: meters
    #              Reference Frame: Global
    #
    #   Author: Kevin Schroeder, PhD
    #   Date:  31 May 2021
    #
    #   NOTE: By default the corners are numbered CW with the first corner in
    #         the lower left when viewing from in front of the gate.
    """
    vec1 = corners[2,:] - corners[0,:]
    vec2 = corners[1,:] - corners[3,:]
    pose = cross(vec1, vec2)

    return pose / norm(pose)


def calcRandomNoise(magnitude: float, dimension: Optional[int] = None) -> Union[float, ndarray]:
    """Calculate Random Noise.

    #   Description:
    #       This function returns a list of random numbers generated from a
    #       normal distribution with a standard deviation prescribed by
    #       'magnitude'. The length of the list is dictated by 'dimension'.
    #
    #       It is expected that this function will be used to generate random
    #       zero-mean Gaussian noise.
    #
    #   Input Variables:
    #       magnitude - the standard deviation of the noise
    #              Size: [1 x 1]
    #       dimension - determines the length of the [1 x n] output array
    #              Size: [1 x 1]
    #
    #   Returned Variables:
    #       noise - an array of random noise scaled by the given magnitude
    #       and of the given length
    #              Size: [1 x n]
    #                   where n = dimension
    #
    #   Author: Kevin Schroeder, PhD
    #   Date:  31 May 2021
    """
    return normal(loc=0.0, scale=magnitude, size=dimension)


def calcVectorFromAngles(pan: float, tilt: float) -> ndarray:
    """Calculate Vector From Angles.

    #   Description:
    #       This function creates a unit vector representation of pan and tilt
    #       angles referenced from the drone's current look vector
    #
    #   Input Variables:
    #       pan - The angle in the x-y plane between the desired point and the
    #       drone's current look vector
    #              Size: [1 x 1]
    #              Units: degrees
    #
    #       tilt - The angle in the z plane between the desired point and the
    #       drone's current look vector
    #              Size: [1 x 1]
    #              Units: degrees
    #
    #   Returned Variables:
    #       vector - the normal vector representation of the given angles centered on
    #       the drone's current look vector
    #              Size: [1 x 3]
    #              Units: unitless
    #              Reference Frame: Drone's Local RFU
    #
    #   Author: Kevin Schroeder, PhD
    #   Date:  31 May 2021
    """
    look_vector = array([0, 1, 0])
    vector = look_vector.dot(rotateVector1(tilt).dot(rotateVector3(pan)))
    return vector


def calcRangeFromTilt(tilt_low: float, tilt_high: float) -> float:
    """Calculate Range From Tilt.

    #   Description:
    #       This function calculates the horizontal distance from the drone to
    #       a gate by the measured angle between drone's current heading and
    #       the top of the gate and the bottom of the gate. Unlike 'calcRange',
    #       this function does not return the length of the vector from the
    #       drone to the center of the gate.
    #
    #   Input Variables:
    #       tiltLow - the angle from the drone's local heading to the
    #           bottom of the gate
    #              Size: [1 x 1]
    #              Units: degrees
    #              Reference Frame: Drone's Local RFU
    #
    #       tiltHigh - the angle from the drone's local heading to the
    #           top of the gate
    #              Size: [1 x 1]
    #              Units: degrees
    #              Reference Frame:  Drone's Local RFU
    #
    #   Returned Variables:
    #       range - the horizontal distance between the drone and the gate.
    #              Size: [1 x 1]
    #              Units: meters
    #
    #   Author: Kevin Schroeder, PhD
    #   Date:  31 May 2021
    #
    #   NOTE: This trig identity assumes the gates are perfectly vertical and
    #         the drone is perfectly level.
    """
    return SIDELENGTH / (tan(radians(tilt_high)) - tan(radians(tilt_low)))
