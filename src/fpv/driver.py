# Standard Imports
from datetime import datetime
# Third Party Imports
from numpy import arange
# Local Imports
from fpv.modules.navigation import findGate, approachGate, flyThroughGate
from fpv.modules.initialization import initializeDrone
from fpv.modules.constants import SIDELENGTH, NUM_GATES


def main():
    """Main driver function for running simulator."""
    # Initialize design variables
    estimate_drone, true_drone = initializeDrone(SIDELENGTH / 2 + 1)

    # Autonomy
    objectives = arange(NUM_GATES)  # gate IDs

    offset = 4  # meters
    overshoot = 8  # meters

    # begin race
    tic = datetime.now()

    for gateID in objectives:
        print('targeting Gate', gateID)

        # Step 1: Find next gate
        _, estimate_drone, true_drone = findGate(gateID, estimate_drone, true_drone)

        # Step 2: Approach next gate
        estimate_drone, true_drone = approachGate(gateID, offset, estimate_drone, true_drone)

        # Step 3: Fly though next gate
        estimate_drone, true_drone = flyThroughGate(gateID, overshoot, estimate_drone, true_drone)

    toc = datetime.now()
    print(f"You completed the course in {toc - tic} seconds")


if __name__ == "__main__":
    main()
