# Standard Library Imports
# Third Party Imports
from numpy import array, deg2rad, cos, sin
# Local Imports


def rotateFrame1(deg: float):
    """."""
    rad = deg2rad(deg)
    return array([[1, 0, 0],
                  [0, cos(rad), -sin(rad)],
                  [0, sin(rad), cos(rad)]])


def rotateFrame2(deg: float):
    """."""
    rad = deg2rad(deg)
    return array([[cos(rad), 0, sin(rad)],
                  [0, 1, 0],
                  [-sin(rad), 0, cos(rad)]])


def rotateFrame3(deg: float):
    """."""
    rad = deg2rad(deg)
    return array([[cos(rad), -sin(rad), 0],
                  [sin(rad), cos(rad), 0],
                  [0, 0, 1]])


def rotateVector1(deg: float):
    """."""
    rad = deg2rad(deg)
    return array([[1, 0, 0],
                  [0, cos(rad), sin(rad)],
                  [0, -sin(rad), cos(rad)]])


def rotateVector2(deg: float):
    """."""
    rad = deg2rad(deg)
    return array([[cos(rad), 0, -sin(rad)],
                  [0, 1, 0],
                  [sin(rad), 0, cos(rad)]])


def rotateVector3(deg: float):
    """Rotate.

    #   =======================================================================
    #
    #   rotateFrame3.m
    #   Description:
    #       This function returns a frame rotation matrix meant to be used to
    #       rotate a frame about the z axis by a certain number of degrees
    #
    #   Input Variables:
    #       deg - the number of degrees to rotate about the z axis by
    #              Size: [1 x 1]
    #              Units: degrees
    #              Reference Frame: Global
    #
    #   Returned Variables:
    #       rotationMatrix - the rotation matrix necessary to rotate
    #       a frame about the z axis by the input number of degrees
    #              Size: [3 x 3]
    #              Units: unitless
    #              Reference Frame: Global
    #
    #   Author: Kevin Schroeder, PhD
    #   Date:  31 May 2021
    #
    #   =======================================================================
    """
    rad = deg2rad(deg)
    return array([[cos(rad), sin(rad), 0],
                  [-sin(rad), cos(rad), 0],
                  [0, 0, 1]])
