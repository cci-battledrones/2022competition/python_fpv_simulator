﻿# Third Party Imports
from numpy import array
from numpy.linalg import norm
# Local imports
from .drone import Drone
from .droneCommands import moveDrone, turnDrone, updateDrone
from .globalFunctions import getGateLocation, getGatePose
from .localFunctions import estimateGateIDs, estimatePanTilt
from .utils import calcAngleBetweenVectors


def approachGate(gateID: int, offset: float, estimate_drone: Drone, true_drone: Drone):
    """This function will direct the drone to a desired gate.

    It first will set an objective on the course map, then fly towards that objective,
    then align itself with the gate.

    #Description:
    #This function will direct the drone to a desired gate. It first will
    #set an objective on the course map, then fly towards that objective,
    #then align itself with the gate.

    #Input Variables:
    #    gateID - Gate identification number, created in buildCourse.m, a
    #    gate's identification number dictates what order the drone will fly
    #    through the gate
    #           Size: [1 x 1]
    #    offset - Desired distance in front of the gate that you want the
    #    drone to be following this function
    #           Size: [1 x 1]
    #           Units: meters

    #Returned Variables:
    #    NONE

    #Author: Kevin Schroeder, PhD
    #Date:  31 May 2021
    """
    # step 1: set objective on map
    gateLocation = array(getGateLocation(gateID))
    gatePose = array(getGatePose(gateID))
    objective = gateLocation + offset * gatePose

    # step 2: go to objective
    range_ = norm(objective - estimate_drone.location)
    desiredHeading = (objective - estimate_drone.location) / norm(objective - estimate_drone.location)
    turnAngle = calcAngleBetweenVectors(estimate_drone.heading, desiredHeading)

    true_drone = turnDrone(true_drone, turnAngle, error=0.0)
    true_drone = moveDrone(true_drone, range_, error=0.0)
    estimate_drone = turnDrone(estimate_drone, turnAngle)
    estimate_drone = moveDrone(estimate_drone, range_)

    # step 3: align with gate
    turnAngle = calcAngleBetweenVectors(estimate_drone.heading, -gatePose)
    true_drone = turnDrone(true_drone, turnAngle, error=0.0)
    estimate_drone = turnDrone(estimate_drone, turnAngle)
    estimate_drone, true_drone = updateDrone(estimate_drone, true_drone)

    # step 4: ensure Target Gate is in View
    isVisible = False
    while not isVisible:
        _, gateIDs, _ = estimateGateIDs(estimate_drone, true_drone)
        isVisible = any(gateIDs == gateID)
        if not isVisible:
            true_drone = moveDrone(true_drone, -5, error=0.0)
            estimate_drone = moveDrone(estimate_drone, -5)
            estimate_drone, true_drone = updateDrone(estimate_drone, true_drone)

    return estimate_drone, true_drone


def findGate(gateID: int, estimate_drone: Drone, true_drone: Drone):
    """This function changes the drone's heading to try and put the desired gate within the camera's view.

    #Description:
    #This function changes the drone's heading to try and put the desired
    #gate within the camera's view.

    #Step 1: While loop will repeatedly loop while a gate is searched for until the
    #desired gate is found

    #Step 2: The drone algorithmically (Munkres algorithm) estimates what the ID of each gate within its
    #current field of view is, this is updated with each iteration

    #Step 3: If requested gate is found, its corners within the camera's field of view
    #are returned. The corners of other gates within the field of view are
    #excluded, should they exist.

    #Step 4: If the requested gate is not found, it will rotate in the direction it
    #expects to find the gate given what gates it can currently see

    #Input Variables:
    #    gateID - Gate identification number, created in buildCourse.m, a
    #    gate's identification number dictates what order the drone will fly
    #    through the gate
    #           Size: [1 x 1]

    #Returned Variables:
    #    projectedCorners - Projected corners of the desired gate
    #           Size: [numCorners x panTilt]
    #                    'numCorners' corresponds to the number of corners (corner
    #                    number, clockwise starting at lower left corner,
    #                    created in buildgate function). There will always
    #                    be 4.
    #                    'panTilt' corresponds to the number of Pan Angles/Tilt
    #                    Angles, this will always be 2.
    #           Units: degrees
    #           Reference Frame: Drone Centered

    #NOTE: Angles are from the look vector of the drone

    #Author: Kevin Schroeder, PhD
    #Date:  31 May 2021
    """
    gateFound = False
    while not gateFound:

        # Step 1: Identify gates in drone's FOV
        allProjectedCorners, gateIDs, _ = estimateGateIDs(estimate_drone, true_drone)

        # Step 2: identify corners of desired gate if it is in view
        if gateID in gateIDs:
            # gateIndex = where(gateID == gateIDs)
            gateIndex = [idx for idx, id_ in enumerate(gateIDs) if id_ == gateID][0]
            projectedCorners = allProjectedCorners[:,:,gateIndex]
            gateFound = True

        # Step 3: if the desired gate is not in view, turn the drone to face the desired gate
        else:
            if gateID is None:
                turnAngle = 10

            # Step 2: if a gateID is specified, calculate the turn angle required to point at the desired gate
            else:
                gateLocation = getGateLocation(gateID)
                turnAngle = estimatePanTilt(gateLocation, estimate_drone)[0]

            # Step 3: Turn the drone
            true_drone = turnDrone(true_drone, turnAngle, error=0.0)
            estimate_drone = turnDrone(estimate_drone, turnAngle)
            estimate_drone, true_drone = updateDrone(estimate_drone, true_drone)

    return projectedCorners, estimate_drone, true_drone


def flyThroughGate(gateID: int, overshoot: float, estimate_drone: Drone, true_drone: Drone):
    """Drone fly through the given gate and overshoot the gate's position by a specified distance.

    #Description:
    #This function lets the drone fly through the given gate and overshoot
    #the gate's position by a specified distance

    #Step 1: Verifies that the gate to be flown through is currently within view

    #Step 2: Calculates how much the simulated drone must turn and how far it must fly forward in
    #order to go through the desired gate

    #Step 3: Turns the simulated drone and then flies it forward by the calculated
    #values, will not execute the motions if the calculated angle is greater
    #than 50 degrees for fear of crashing into the gate

    #Input Variables:
    #    gateID - Gate identification number, created in buildCourse.m, a
    #    gate's identification number dictates what order the drone will fly
    #    through the gate
    #           Size: [1 x 1]
    #    overshoot - Overshoot of the gate's position. This is done such
    #    that the drone can be deposited a specified distance away from the
    #    gate as to not have any risk of colliding with it as it flies to
    #    the next one
    #           Size: [1 x 1]
    #           Units: meters

    #Returned Variables:
    #    NONE

    #Author: Kevin Schroeder, PhD
    #Date:  31 May 2021
    """
    # Step 1: Align with gate
    gateLocation = getGateLocation(gateID)
    vector2Gate = gateLocation - estimate_drone.location
    gate_range = norm(vector2Gate)
    turnAngle = calcAngleBetweenVectors(estimate_drone.heading, vector2Gate)
    estimate_drone = turnDrone(estimate_drone, turnAngle)
    true_drone = turnDrone(true_drone, turnAngle, error=0.0)

    # Step 2: if Drone and gate are poorly aligned approach gate again
    if abs(turnAngle) > 40:
        offset = 10
        approachGate(gateID, offset, estimate_drone, true_drone)
        vector2Gate = gateLocation - estimate_drone.location
        gate_range = norm(vector2Gate)

    # Step 3: Fly through the gate by the desired overshoot amount
    true_drone = moveDrone(true_drone, gate_range + overshoot, error=0.0)
    estimate_drone = moveDrone(estimate_drone, gate_range + overshoot)
    estimate_drone, true_drone = updateDrone(estimate_drone, true_drone)
    return estimate_drone, true_drone
