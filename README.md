# FPV Drone Simulation

Requires:
- Python >= 3.7
- numpy >= 1.19
- scipy >= 1.6
- matplotlib >= 3.3

## Installation

Once a valid Python is installed, run the following command from this directory:

```shell
pip install -e .
```

This will install the dependencies and the simulator properly.


## Usage

To run the simulator execute the following:

```shell
fpv-sim
```

You may change the behavior by editing source code files located under the **src/** directory.
See **src/driver.py** for the main simulation script.
