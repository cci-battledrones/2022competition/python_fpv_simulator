from typing import Tuple
# Third Party Imports
from numpy import array, mean, cross, sqrt, zeros, sum as np_sum, ndarray
from numpy.linalg import norm
from scipy.optimize import linear_sum_assignment as munkres
# Local Imports
from .drone import Drone
from .globalFunctions import getGateCorners, getFrame
from .initialization import COURSEMAP
from .utils import calcVectorFromAngles, calcPanTilt, calcRangeFromTilt


def estimateGateIDs(estimate_drone: Drone, true_drone: Drone) -> Tuple[ndarray, ndarray, ndarray]:
    """Estimate the Gate ID's.

    This function looks at the visible gates using the 'getFrame()'
    function which emulates the output from the onboard computer vision.
    The function assigns gateIDs to the visible gates using the Munkres
    algorithm and returns the assigned and unassigned gateIDs and the pan
    and tilt angles to the corners of all visible gates. The pan and tilt
    angles of the corners of each visible gate are associated with their
    corresponding gateID.
    The function pulls the map information from the MATLAB GLOBAL workspace
    to compare the estimated location of the visible gates to the expected
    location of each gate on the map. To compare the location of gates, the
    function compares the estimated and expected range and pan angle of the
    gates.
    #   Input Variables:
    #       , estimate_drone, true_drone
    #
    #   Returned Variables:
    #
    #       allProjectedCorners - a 3 dimensional matrix, corresponding to the
    #       pan and tilt angles of the corners of all visible gates. Angles
    #       are measured from the drone's 'look vector' to each gate corner.
    #       Each page of the matrix corresponds to each visible gate and is the
    #       row index of 'gateIDs' for the associated gate. The rows of each
    #       page correspond to the number of corners and the first and second
    #       columns of each page contain the pan and tilt angles of all corners
    #       respectively.
    #
    #       Example: allProjectedCorners(:,1,2) returns pan angles of all
    #                corners of the second visible gate
    #
    #       Examples: allProjectedCorners(:,2,4) returns tilt angles of all
    #                 corners of the fourth visible gate.
    #
    #              Size: [numCorners x numAngles x numberOfGates]
    #                       'numCorners' corresponds to the number of corners
    #                                    and will always be equal to 4
    #                       'numAngles' corresponds to the pan and tilt angles
    #                                   respectively and is always equal to 2
    #                       'numberOfGates' corresponds to the number of number
    #                                       of visible gates
    #
    #              Units: degrees
    #              Reference Frame: Drone Centered
    #
    #       gateIDs - a column vector corresponding to the gateID of the
    #                 visible gates. The row index of each value in gateIDs
    #                 corresponds to the associated gate in
    #                 'allProjectedCorners'.
    #                 Example: allProjectedCorners(:,:,gateIDs==3) returns the
    #                          pan and tilt angles of all corners of the gate
    #                          with the gateID of 3.
    #
    #              Size: [numberOfGates x 1]
    #                       'numberOfGates' corresponds to the number of
    #                                       visible gates
    #              Units: None
    #
    #       unassigned - a column vector corresponding to the
    #                    unassigned gateIDs
    #
    #              Size: [numberOfGates x 1]
    #                       'numberOfGates' corresponds to the number of number
    #                                       of visible gates
    #
    #               Units: None
    #               Reference Frame: None
    #
    #   NOTE: The number of assigned and unassigned gateIDs will always add up
    #         to 10, which corresponds to the total number of gates on the map.
    #
    #   NOTE: To learn more about the Munkres algorithm, type
    #         'help assignmunkres' into MATLAB's Command Window or follow the
    #          examples on mathworks.com at:
    #         'https://www.mathworks.com/help/fusion/ref/assignmunkres.html'
    #
    #   NOTE: To learn more about how the function estimates the location of
    #         gates, look at the functions in the Local Function folder.
    #
    #   NOTE: To learn more about how the function finds the expected location
    #         of gates, look at the functions in the Global Function folder
    #
    #   NOTE: If no gates are visible in the camera's field of view, the
    #         function will use the 'searchForGate()' function to turn the drone
    #         by 10 degrees before looking for visible gates using the
    #         'getFrame()' function.
    #
    #   Author: Kevin Schroeder, PhD
    #   Date:  31 May 2021
    """
    # Step 1 Predict: Use the map to predict where gates should be
    predictedRange = zeros((10,))
    predictedPan = zeros((10,))
    for gateID in range(COURSEMAP.shape[0]):
        corners = getGateCorners(gateID)
        numCorners = corners.shape[0]
        predictedPanTilts = zeros((numCorners, 2))
        for ii in range(numCorners):
            predictedPanTilts[ii, 0], predictedPanTilts[ii, 1] = estimatePanTilt(corners[ii, :], estimate_drone)

        predictedRange[gateID] = estimateGateRange(predictedPanTilts)
        predictedPan[gateID] = mean(predictedPanTilts[:, 0])

    ## Step 2 Measure: Measure where gates are
    measuredPanTilts = getFrame(true_drone)
    numberOfGates = measuredPanTilts.shape[0]

    measuredRange = zeros((numberOfGates,))
    measuredPan = zeros((numberOfGates,))
    if numberOfGates != 0:
        for gate in range(numberOfGates):
            measuredRange[gate] = estimateMeasuredGateRange(measuredPanTilts[gate])
            measuredPan[gate] = mean(measuredPanTilts[gate, 0, :])

        # Step 3 Correlate: expectedCorners and trackedCorners
        prediction = array([predictedRange.T, predictedPan.T]).T
        measurements = array([measuredRange.T, measuredPan.T]).T
        costMatrix = zeros((prediction.shape[0], numberOfGates))
        for jj in range(prediction.shape[0]):
            delta = measurements - prediction[jj, :]
            costMatrix[jj, :] = sqrt(np_sum(delta**2, 1))

        assignments, _ = munkres(costMatrix)
        # NOTE: adding 1 for the relationsip between python indexing and gate number
        gateIDs = assignments
        unassigned = []
    else:
        gateIDs = []
        unassigned = list(range(10))

    return measuredPanTilts, gateIDs, unassigned


def estimateGateLocation(projectedCorners: ndarray) -> ndarray:
    """Estimate Gate Location.

    This function estimates the vector to the gate's center from the
    center of the drone. The function references the 'estimateGateRange'
    function from the Local Function folder for calculating the magnitude
    of the vector and it references the 'calcVectorFromAngles' function
    from the Utilities folder for calculating the direction of the vector.
    The returned location vectors is in the Drone's local right-forward-up
    coordinate frame (RFU).
    #
    #   Input Variables:
    #
    #       projectedCorners - a 4x2 matrix, corresponding to the
    #       pan and tilt angles of the corners of the gate. Angles
    #       are measured from the drone's 'look vector' to each gate corner.
    #       The first and second columns of projectedCorners contain the pan
    #       and tilt angles of all corners respectively.
    #
    #       Example: projectedCorners(:,1) returns pan angles of all corners
    #
    #       Examples: projectedCorners(:,2) returns tilt angles of all corners
    #
    #              Size: [numCorners x numAngles]
    #                       'numCorners' corresponds to the number of corners
    #                                    and will always be equal to 4
    #                       'numAngles' corresponds to the pan and tilt angles
    #                                   respectively and is always equal to 2
    #
    #              Units: degrees
    #              Reference Frame: Drone Centered
    #
    #   Returned Variables:
    #
    #       gateRelativeLocation - a row vector to the center of the
    #       gate from the center of the drone in the right-forward-up
    #       coordinate frame (RFU)
    #
    #              Size: [1 x dim]
    #                       'dim' corresponds to the number dimensions and is
    #                        always equal to 3
    #
    #              Units: meters
    #              Reference Frame: Drone Centered
    #
    #   NOTE: The returned vector is in the Drone's local right-forward-up
    #   coordinate frame (RFU)
    #   NOTE: To learn more about calculating the magnitude of the location
    #         vector look at the 'estimateGateRange' function in the Local Function
    #         folder
    #   NOTE: To learn more about calculating the direction of the location
    #         vector look at the 'calcVectorFromAngles' function in the
    #         Utilities folder.
    #
    #   Author: Kevin Schroeder, PhD
    #   Date:  31 May 2021
    """
    ## Step 1: Calculate mean pan and tilt angles of the corners
    panAngles = projectedCorners[0]
    meanPan = mean(panAngles)

    tiltAngles = projectedCorners[1]
    meanTilt = mean(tiltAngles)
    ## Step 2: Calculate the distance to the center of the gate
    gate_range = estimateMeasuredGateRange(projectedCorners)
    ## Step 3: Calculate the vector to the center of the gate
    gateRelativeLocation = gate_range * calcVectorFromAngles(meanPan, meanTilt)

    return gateRelativeLocation


def estimateGatePose(projectedCorners: ndarray) -> ndarray:
    """Estimate Gate Pose.

    This function estimates the gate's normal vector which corresponds to
    the unit vector of the gate's orientation in the Drone's local
    right-forward-up coordinate frame (RFU). This vector points out of the
    front side of the gate. The function references the functions in the
    Utilities folder to calculate the range and direction to the left and
    right side of the gate, and utilizes trigonometric identities to
    estimate gate's normal vector.
    #
    #   Input Variables:
    #
    #       projectedCorners - a 4x2 matrix, corresponding to the
    #       pan and tilt angles of the corners of the gate. Angles
    #       are measured from the drone's 'look vector' to each gate corner.
    #       The first and second columns of projectedCorners contain the pan
    #       and tilt angles of all corners respectively.
    #
    #       Example: projectedCorners(:,1) returns pan angles of all corners
    #
    #       Examples: projectedCorners(:,2) returns tilt angles of all corners
    #
    #              Size: [numCorners x numAngles]
    #                       'numCorners' corresponds to the number of corners
    #                                    and will always be equal to 4
    #                       'numAngles' corresponds to the pan and tilt angles
    #                                   respectively and is always equal to 2
    #
    #              Units: degrees
    #              Reference Frame: Drone Centered
    #
    #   Returned Variables:
    #
    #       poseVector - a unit row vector corresponding to the the gate's
    #                    orientation in the Drone's local right-forward-up
    #                    coordinate frame (RFU).
    #
    #              Size: [1 x dim]
    #                       'dim' corresponds to the number dimensions and is
    #                        always equal to 3
    #
    #               Units: None
    #               Reference Frame: Drone Centered
    #
    #   NOTE: A unit vector has a magnitude of 1 and only represents a
    #         direction
    #   NOTE: A range vector is calculated as: rangeVec = rangeMag*rangeUnit
    #         where rangeMag is the magnitude of the vector and rangeUnit is
    #         its direction.
    #   NOTE: Vec/norm(Vec) calculates a unit vector that carries the direction
    #         of Vec
    #
    #   Author: Kevin Schroeder, PhD
    #   Date:  31 May 2021
    """
    ## Step 1: Estimate vector to the left and right side of the gate
    panAngles = projectedCorners[0]
    tiltAngles = projectedCorners[1]
    rangeMag1 = calcRangeFromTilt(tiltAngles[0], tiltAngles[1])
    rangeUnit1 = calcVectorFromAngles(mean(panAngles[0:1]), mean(tiltAngles[0:1]))
    rangeVec1 = rangeMag1 * rangeUnit1

    rangeMag2 = calcRangeFromTilt(tiltAngles[3], tiltAngles[2])
    rangeUnit2 = calcVectorFromAngles(mean(panAngles[2:3]), mean(tiltAngles[0:1]))
    rangeVec2 = rangeMag2 * rangeUnit2

    ## Step 2: Find the vector that goes from the left to right side of the gate
    # [NOTE]: left2Right will be +1 if it is rangeVec1 points to the left side
    #      and -1 if rangeVec1 points to the right side (from the drone's perspective).
    if panAngles[0] > panAngles[3]:
        left2Right = 1
    else:
        left2Right = -1

    gateWidthVector = left2Right * (rangeVec2 - rangeVec1)

    ## Step 3: Derive the pose vector of the gate
    upVector = array([0, 0, 1])
    poseVector = cross(gateWidthVector, upVector) / norm(gateWidthVector)

    return poseVector


def estimateGateRange(projectedCorners: ndarray) -> float:
    """Estimate gate range.

    This function estimates the distance to the gate's center from the
    center of the drone. The function utilizes tilt angles and pulls
    trigonometric identities from the functions in the Utilities folder to
    estimate the drone's distance to center of the gate. The returned
    distance is a scalar quantity with units of meters.
    #
    #   Input Variables:
    #
    #       projectedCorners - a 4x2 matrix, corresponding to the
    #       pan and tilt angles of the corners of the gate. Angles
    #       are measured from the drone's 'look vector' to each gate corner.
    #       The first and second columns of projectedCorners contain the pan
    #       and tilt angles of all corners respectively.
    #
    #       Example: projectedCorners(:,1) returns pan angles of all corners
    #
    #       Examples: projectedCorners(:,2) returns tilt angles of all corners
    #
    #              Size: [numCorners x numAngles]
    #                       'numCorners' corresponds to the number of corners
    #                                    and will always be equal to 4
    #                       'numAngles' corresponds to the pan and tilt angles
    #                                   respectively and is always equal to 2
    #
    #              Units: degrees
    #              Reference Frame: Drone Centered
    #
    #   Returned Variables:
    #       range - a scalar quantity corresponding to the distance to the
    #       gate's center from the center of the drone.
    #              Size: [1 x 1]
    #
    #              Units: meters
    #              Reference Frame: Drone Centered
    #
    #   NOTE: The returned range is a scalar quantity.
    #   NOTE: To learn more about range calculation look at the
    #         'calcRangeFromTilt' function in the Utilities folder.
    #
    #   Author: Kevin Schroeder, PhD
    #   Date:  31 May 2021
    """
    ## Step 1: Calculate the distance to the left and right sides of the gate
    tiltAngles = projectedCorners[:, 1]
    range1 = calcRangeFromTilt(tiltAngles[0], tiltAngles[1])
    range2 = calcRangeFromTilt(tiltAngles[3], tiltAngles[2])

    ## Step 2: Calculate the distance to the center of the gate
    gate_range = (range1 + range2) / 2

    return gate_range


def estimateMeasuredGateRange(projectedCorners: ndarray) -> float:
    """Estimate Measured gate range."""
    ## Step 1: Calculate the distance to the left and right sides of the gate
    tiltAngles = projectedCorners[1]
    range1 = calcRangeFromTilt(tiltAngles[0], tiltAngles[1])
    range2 = calcRangeFromTilt(tiltAngles[3], tiltAngles[2])

    ## Step 2: Calculate the distance to the center of the gate
    gate_range = (range1 + range2) / 2

    return gate_range


def estimatePanTilt(points: ndarray, estimate_drone: Drone) -> Tuple[ndarray, ndarray]:
    """Estimate Pan Tilt.

    This function estimates the pan and tilt angles from the current
    'look vector' of the drone to the provided points. The pan angle is
    and angle from drone's 'look vector' to the projection of the point on
    the drone's local horizon and the tilt angle is the angle up or down
    from drone's look vector to the point. The function pulls the estimated
    drone heading and the estimated location of the drone from the MATLAB
    GLOBAL workspace and applies trigonometric identities to calculate the
    pan and tilt angle.
    #
    #   Input Variables:
    #       points - a list of row vectors from the map's origin to the
    #                desired point. The vector is in the Global
    #                reference frame
    #              Size: [numPoints x dim]
    #                       'numPoints' corresponds to the number of points
    #                       'dim' corresponds to the number dimensions and is
    #                        always equal to 3
    #
    #              Units: meters
    #              Reference Frame: Global
    #
    #   Returned Variables:
    #       pan - a column vector representing the angle (in degrees) across
    #             the drone's local horizon(i.e., the Right-Front plane) from
    #             the drone's current'lookVector' to the a projection  of the
    #             'point' on to the drone's local horizon.
    #
    #              Size: [numPoints x 1]
    #                       'numPoints' corresponds to the number of points
    #
    #              Units: degrees
    #              Reference Frame: Drone Centered
    #
    #       tilt - A column vector representing the angle (in degrees) up or
    #              down from the drone's local horizon to the provided 'point'
    #
    #              Size: [numPoints x 1]
    #                       'numPoints' corresponds to the number of points
    #
    #              Units: degrees
    #              Reference Frame: Drone Centered
    #
    #   NOTE: Vec/norm(Vec) calculates a unit vector that carries the direction
    #         of Vec
    #   NOTE: The pan and tilt angles are measured relative to the current
    #         'look vector' of the drone
    #   NOTE: To learn more about the trigonometric identities used in this
    #         function, look at 'calcPanTilt' function in the Utilities folder.
    #
    #   Author: Kevin Schroeder, PhD
    #   Date:  31 May 2021
    """
    ## Step 2: Calculate pan and tilt angles from drone's current 'look vector' to each point:
    # if points.ndim == 1:
    #     numPoints = 1
    # else:
    #     numPoints = points.shape[0]
    # pan = zeros((numPoints,))
    # tilt = zeros((numPoints,))
    # for ii in range(numPoints):
    # Note: Look at 'calcPanTilt' function in the Utilities folder to
    #      learn more about the applied trigonometric identities.
    directionVector = (points - estimate_drone.location) / norm(points - estimate_drone.location)
    pan, tilt = calcPanTilt(estimate_drone.heading, directionVector)

    return pan, tilt
