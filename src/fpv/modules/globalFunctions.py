# Standard Imports
from typing import Tuple
# Third Party Imports
from numpy import mean, zeros, logical_and, ndarray
from scipy.linalg import norm
# Local Imports
from .drone import Drone
from .utils import calcPose, calcPanTilt
from .initialization import COURSEMAP
from .constants import FOVANGLE


def getGateCorners(gateID: int) -> ndarray:
    """Get Gate Corners.

    #   Description:
    #   This function pulls information from the MATLAB GLOBAL workspace. It
    #   finds the position of a specific gate's corners (known a priori).
    #
    #   It is expected that this function can be used to identify unknown gates
    #   or to derive the drone's current location based on its relative
    #   location to a known (i.e., identified) gate.
    #
    #   Input Variables:
    #       gateID      - The numeric value of the desired gate (i.e., 1-10)
    #
    #   Returned Variables:
    #       corners     -   a list of 4 row vectors cooresponding to the four
    #                       corners of the desired gate. The corners are listed
    #                       clockwise from the bottom left (as seen when
    #                       looking at the front of the gate)
    #
    #   NOTE: the returned vectors are in the Global coordinate frame
    #
    #   Author: Kevin Schroeder, PhD
    #   Date:  31 May 2021
    """
    corners = COURSEMAP[gateID]
    return corners


def getGateLocation(gateID: int) -> ndarray:
    """Get Gate Location.

    #   Description:
    #   This function pulls information from the MATLAB GLOBAL workspace. It
    #   finds the mean position of a specific gate's corners (known a priori).
    #   Unlike 'estimateGateLocation', this function pulls the true gate
    #   location from the map. This is further different as it reports the gate
    #   location in the Global coordinate frame, measured from the map's origin
    #   whereas 'estimateGateLocation' estimates the vector to the gate's
    #   center from the drone in the Drone's local right-forward-up coordinate
    #   frame (RFU)
    #
    #   It is expected that this function can be used to identify unknown gates
    #   or to derive the drone's current location based on its relative
    #   location to a known (i.e., identified) gate.
    #
    #   Input Variables:
    #       gateID          - The numeric value of the desired gate (i.e., 1-10)
    #
    #   Returned Variables:
    #       gateLocation    - a row vector to the center of the desired gate
    #                         from the map's origin
    #
    #   NOTE: the returned vector is in the Global coordinate frame
    #
    #   Author: Kevin Schroeder, PhD
    #   Date:  31 May 2021
    """
    corners = getGateCorners(gateID)
    gateLocation = mean(corners, 0)
    return gateLocation


def getGatePose(gateID: int) -> ndarray:
    """Get Gate Pose.

    #   Description:
    #   This function pulls information from the MATLAB GLOBAL workspace. It
    #   finds the unit vector of the gate's orientation, which points along the
    #   gate's normal vector. Unlike 'estimateGatePose', this function pulls
    #   the true gate orientation from the map. This is further different as it
    #   reports the gate orientation in the Global coordinate frame, whereas
    #   'estimateGatePose' estimates the vector to the gate's orientation in
    #   the Drone's local right-forward-up coordinate frame (RFU)
    #
    #   It is expected that this function can be used to identify unknown gates
    #   or to derive the drone's current location based on its relative
    #   location to a known (i.e., identified) gate.
    #
    #   Input Variables:
    #       gateID - Gate identification number, created in 'buildCourse'. A
    #       gate's identification number dictates what order the drone will fly
    #       through the gate
    #              Size: [1 x 1]
    #
    #   Returned Variables:
    #       poseVector - a unit vector normal to the plane of the gate. The
    #           vector points towards the viewer when viewed from the front
    #           of the gate
    #              Size: [1 x 3]
    #              Units: unitless
    #              Reference Frame: Map's Global RFU
    #
    #   NOTE: the returned vector is in the Global coordinate frame
    #
    #   Author: Kevin Schroeder, PhD
    #   Date:  31 May 2021
    """
    corners = getGateCorners(gateID)
    poseVector = calcPose(corners)
    return poseVector


def getFrame(true_drone: Drone) -> ndarray:
    """Get Frame.

    #   Description:
    #   This function emulates the output from the onboard computer vision. It
    #   identifies the gates and their associated corners that are in view of
    #   the camera. This function accesses information from the MATLAB GLOBAL
    #   workspace, but it does not return any information that should not be
    #   know onboard the drone (i.e., corners and gate returned by this
    #   function are disassociated from the corners and gate information in the
    #   MAP).
    #
    #   It is expected that this function simulate the measurements that can be
    #   made from the camera.
    #
    #   Input Variables:
    #       NONE
    #
    #   Returned Variables:
    #       projectedCorners - Projected corners of the visible gates
    #              Size: [numCorners x panTilt x numGates]
    #                       'numCorners' corresponds to the number of corners (corner
    #                       number, clockwise starting at lower left corner,
    #                       created in 'buildGate'). Typically there will always
    #                       be 4 corners.
    #                       'panTilt' will always have a dimension of two. Dimension
    #                       one corresponds to the pan angles and dimension two to
    #                       the tilt angles.
    #                          NOTE: The first column  will always be the pan angle
    #                          and the second will be tilt.
    #                       'numGates' corresponds to the number of gates that are
    #                       fully in the field of view of the camera.
    #              Units: degrees
    #
    #   Author: Kevin Schroeder, PhD
    #   Date:  31 May 2021
    """
    # Step 1: Calculate the pan and tilt angles of all gate corners
    numberOfGates = COURSEMAP.shape[0]
    panAngles = zeros((4, numberOfGates))
    tiltAngles = zeros((4, numberOfGates))
    for gateID in range(numberOfGates):
        panAngles[:, gateID], tiltAngles[:, gateID] = getPanTilt(getGateCorners(gateID), true_drone)

    # Step 2: Create a Boolean list of visible corners
    isVisible = zeros((4, numberOfGates))
    isVisible = logical_and(abs(panAngles) < FOVANGLE / 2, abs(tiltAngles) < FOVANGLE / 2)
    # [NOTE]: This `repmat` call seemed redundant
    # fullyVisible = repmat(all(isVisible,1),[4, 1])

    fullyVisible = []
    for col in range(numberOfGates):
        if all(flag is True for flag in isVisible[:, col].tolist()):
            fullyVisible.append(col)
    # Step 3: Return the corner pan and tilt angles of the fully visible gates
    # NOTE: Assume the CV algorithm will not identify gate corners unless the
    # whole gate is in frame
    # projectedCorners = zeros((2, len(fullyVisible) * 4)).T
    projected_corners = zeros((len(fullyVisible), 2, 4))
    for gate, val in enumerate(fullyVisible):
        # index = gate * 4
        # projectedCorners[index:index + 4, 0] = panAngles[:, val]
        # projectedCorners[index:index + 4, 1] = tiltAngles[:, val]
        projected_corners[gate,0,:] = panAngles[:, val]
        projected_corners[gate,1,:] = tiltAngles[:, val]
    #  reshape the 'projectedCorners' array mirror the structure of COURSEMAP:
    # [numGates x numCorners x panTilt] or (numVisibleGates, 4, 2)
    # code = projectedCorners.reshape((4, 2, projectedCorners.shape[0] // 4)).copy()
    return projected_corners


def getPanTilt(points: ndarray, drone: Drone) -> Tuple[ndarray, ndarray]:
    """Get pan tilt.

    %   Description:
    %   This function pulls information about the MATLAB axes (i.e.,
    %   the MATLAB figure).
    %
    %   It is expected that this function can be used to support functions from
    %   the "Simulate Camera" folder, especially in the process of calculating
    %   the pan and tilt angles of gate corners, but may have other uses as
    %   well.
    %
    %   Input Variables:
    %       point   - the position row vector from the map's origin to the
    %                   desired point
    %       NOTE: the input vector should be in the Global coordinate frame
    %
    %   Returned Variables:
    %       pan - the angle (in degrees) across the drone's local horizon
    %           (i.e., the Right-Front plane) from the drone's current
    %           'lookVector' to the a projection of the 'point' on to
    %           the drone's local horizon. Positive angles are CCW
    %		    rotations (defined by the Right-hand Rule).
    %               Size: ['numPoints' x 1]
    %                   'numPoints' corresponds to the number of points
    %               Units: degrees

    %
    %       tilt    - the angle (in degrees) up or down from the drone's local
    %                 horizon to the provided 'point'
    %
    %   NOTE: Following the right-hand convention, clockwise rotations are
    %   negative. Therefore, pan angles are negative if the 'point' is right of
    %   the FPV camera and tilt angles are negative if the 'point' is below the
    %   FPV camera (when viewed from the camera)
    %
    %   Author: Kevin Schroeder, PhD
    %   Date:  31 May 2021
    """
    numPoints = points.shape[0]
    pan = zeros((numPoints,))
    tilt = zeros((numPoints,))
    for ii in range(numPoints):
        directionVector = (points[ii,:] - drone.location) / norm(points[ii,:] - drone.location)
        pan[ii], tilt[ii] = calcPanTilt(drone.heading, directionVector)

    return pan, tilt
